"use strict";
// class Custmer{
//    fname:string;
//    lname:string;
//    constructor(tf:string,tl:string){
//  this.fname=tf;
//  this.lname=tf;
//    }
// }
// let myCustmer = new Custmer("papa","sene");
// console.log(myCustmer.fname);
// console.log(myCustmer.lname);
class Custmer {
    constructor(_fname, _lname) {
        this._fname = _fname; 
        this._lname = _lname;
    }
    get fname() {
        return this._fname;
    }
    set fname(value) {
        this._fname = value;
    }
    get lname() {
        return this._lname;
    }
    set lname(value) {
        this._lname = value;
    }
}
let myCustmer = new Custmer("papa", "sene");
console.log(myCustmer.fname);
console.log(myCustmer.lname);
