"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Cicle_1 = require("./Cicle");
const Rectangle_1_1 = require("./Rectangle.1");
let myCicle = new Cicle_1.Cicle(12, 43, 60);
let myRectangle = new Rectangle_1_1.Rectangle(12, 43, 5, 4);
let theShapes = [];
theShapes.push(myCicle);
theShapes.push(myRectangle);
for (let shaps of theShapes) {
    console.log(shaps.getInfo());
    console.log(shaps.calculate());
}
