import {Shape} from "./Shape"
import { Cicle } from "./Cicle"
import { Rectangle } from "./Rectangle";

let myShape = new Shape(12,43)
console.log(myShape.getInfo());

let myCicle = new Cicle(12,43,60)
console.log(myCicle.getInfo());

let myRectangle = new Rectangle(12,43,5,4)
console.log(myRectangle.getInfo());

