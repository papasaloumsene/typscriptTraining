import {Shape} from "./Shape";
import {Cicle} from "./Cicle"
import {Rectangle} from "./Rectangle"

let myShape = new Shape(12,43)
let myCicle = new Cicle(12,43,60)
let myRectangle = new Rectangle(12,43,5,4)


let theShapes: Shape[] = [];

theShapes.push(myShape);
theShapes.push(myCicle);
theShapes.push(myRectangle);

for(let shaps of theShapes){
    console.log(shaps.getInfo());
}


