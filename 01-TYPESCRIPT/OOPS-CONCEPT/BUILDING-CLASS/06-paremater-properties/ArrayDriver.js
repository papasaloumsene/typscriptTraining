"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Shape_1 = require("./Shape");
const Cicle_1 = require("./Cicle");
const Rectangle_1 = require("./Rectangle");
let myShape = new Shape_1.Shape(12, 43);
let myCicle = new Cicle_1.Cicle(12, 43, 60);
let myRectangle = new Rectangle_1.Rectangle(12, 43, 5, 4);
let theShapes = [];
theShapes.push(myShape);
theShapes.push(myCicle);
theShapes.push(myRectangle);
for (let shaps of theShapes) {
    console.log(shaps.getInfo());
}
