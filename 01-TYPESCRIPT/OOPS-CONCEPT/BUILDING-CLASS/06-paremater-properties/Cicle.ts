import {Shape} from"./Shape"
export class Cicle extends Shape{

    constructor(TheX:number,TheY :number,
            private _radius:number){

                super(TheX,TheY)
            }

    public get radius():number{
        return this,this._radius
    }

    public set radius(value:number){
        this._radius=value;
    }

    getInfo() :string {
        return super.getInfo()+`, the radius ${this._radius }`
    }

}