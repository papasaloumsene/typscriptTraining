"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Cicle = void 0;
const Shape_1 = require("./Shape");
class Cicle extends Shape_1.Shape {
    constructor(TheX, TheY, _radius) {
        super(TheX, TheY);
        this._radius = _radius;
    }
    get radius() {
        return this, this._radius;
    }
    set radius(value) {
        this._radius = value;
    }
    getInfo() {
        return super.getInfo() + `, the radius ${this._radius}`;
    }
}
exports.Cicle = Cicle;
