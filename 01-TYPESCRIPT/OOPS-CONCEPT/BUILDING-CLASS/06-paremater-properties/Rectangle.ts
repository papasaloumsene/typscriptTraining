import { Shape } from "./Shape";
export class Rectangle extends Shape{


    constructor(TheX:number,TheY:number,
        private _width: number,private _height: number){

            super(TheX,TheY)
        }


    public get width(): number {
        return this._width;
    }
    public set width(value: number) {
        this._width = value;
    }

    public get height(): number {
        return this._height;
    }
    public set height(value: number) {
        this._height = value;
    }

    getInfo(){

        return super.getInfo() + `the width=${this._width}, the height=${this._height}`
    }
    
}